from pydantic import BaseSettings


class Settings(BaseSettings):
    TRAIN_DATA_PATH: str = "data/trainingImages/"
    TRAIN_BOX_FILENAME: str = "data/boundingbox.txt"
    TRAIN_SHAPE_FILENAME: str = "data/keypoints.txt"
    TRAIN_CANDIDATE_PIXEL_NUM: int = 400
    TRAIN_FERN_PIXEL_NUM: int = 5
    TRAIN_FIRST_LEVEL_NUM: int = 10
    TRAIN_SECOND_LEVEL_NUM: int = 500
    TRAIN_IMG_NUM: int = 1345

    TEST_DATA_PATH: str = "data/testImages/"
    TEST_BOX_FILENAME: str = "data/boundingbox_test.txt"
    TEST_IMG_NUM: int = 507

    FA_MODEL_PATH: str = "fa_model/"
    FA_RES_PATH: str = "fa_res/"
    LANDMARK_NUM: int = 29
    INITIAL_NUMBER: int = 20

settings = Settings()
print(settings.json(indent=4))