from typing import List

import numpy as np
import cv2

from face_alignment.bounding_box import BoundingBox
from face_alignment.utils import calculate_covariance


class Fern:
    __slots__ = [
        "__fern_pixel_num", "__landmark_num", "__selected_nearest_landmark_index",
        "__threshold", "__selected_pixel_index", "__selected_pixel_locations", "__bin_output"
        ]

    def train(
        self,
        candidate_pixel_intensity: List[List[float]],
        covariance: np.ndarray[float],
        candidate_pixel_locations: np.ndarray[float],
        nearest_landmark_index: np.ndarray[int],
        regression_targets: List[np.ndarray[float]],
        fern_pixel_num: int,
    ) -> List[np.ndarray[float]]:
        self.__fern_pixel_num = fern_pixel_num
        self.__landmark_num = regression_targets[0].shape[0]
        self.__selected_pixel_index = np.zeros((fern_pixel_num, 2), dtype=int)
        self.__selected_pixel_locations = np.zeros((fern_pixel_num, 4))
        self.__selected_nearest_landmark_index = np.zeros((fern_pixel_num, 2), dtype=int)
        candidate_pixel_num = candidate_pixel_locations.shape[0]

        self.__threshold = np.zeros((fern_pixel_num, 1))

        for i in range(fern_pixel_num):
            random_direction = np.random.uniform(
                low=-1.1, 
                high=1.1, 
                size=(self.__landmark_num, 2)
            )
            cv2.normalize(src=random_direction, dst=random_direction)

            projection_result = [
                np.sum(r_target * random_direction) for r_target in regression_targets
            ]

            covariance_projection_density = [
                calculate_covariance(projection_result, cpi) for cpi in candidate_pixel_intensity
            ]

            max_correlation, max_pixel_index_1, max_pixel_index_2 = -1, 0, 0

            for j in range(candidate_pixel_num):
                for k in range(candidate_pixel_num):
                    temp1 = covariance[j, j] + covariance[k, k] - 2 * covariance[j, k]
                    if abs(temp1) < 1e-10:
                        continue

                    flag = False

                    for p in range(i):
                        if (
                            (j == self.__selected_pixel_index[p, 0]
                            and k == self.__selected_pixel_index[p, 1])
                            or (j == self.__selected_pixel_index[p, 1]
                            and k == self.__selected_pixel_index[p, 0])
                        ):
                            flag = True
                            break

                    if flag:
                        continue

                    temp = (covariance_projection_density[j]-covariance_projection_density[k]) / np.sqrt(temp1)

                    if abs(temp) > max_correlation:
                        max_correlation = temp
                        max_pixel_index_1 = j
                        max_pixel_index_2 = k

            self.__selected_pixel_index[i] = (max_pixel_index_1, max_pixel_index_2)
            self.__selected_pixel_locations[i] = (
                candidate_pixel_locations[max_pixel_index_1, 0],
                candidate_pixel_locations[max_pixel_index_1, 1],
                candidate_pixel_locations[max_pixel_index_2, 0],
                candidate_pixel_locations[max_pixel_index_2, 1],
            )
            self.__selected_nearest_landmark_index[i] = (
                nearest_landmark_index[max_pixel_index_1],
                nearest_landmark_index[max_pixel_index_2],
            )

            max_diff = -1 
            for j in range(len(candidate_pixel_intensity[max_pixel_index_1])):
                temp = candidate_pixel_intensity[max_pixel_index_1][j] - candidate_pixel_intensity[max_pixel_index_2][j]
                if abs(temp) > max_diff:
                    max_diff = abs(temp)

            self.__threshold[i] = np.random.uniform(-0.2 * max_diff, 0.2 * max_diff)

        bin_num = 2**self.__fern_pixel_num
        shapes_in_bin = [[]] * bin_num
        for i in range(len(regression_targets)):
            index = 0
            for j in range(self.__fern_pixel_num):
                density_1 = candidate_pixel_intensity[self.__selected_pixel_index[j, 0]][i]
                density_2 = candidate_pixel_intensity[self.__selected_pixel_index[j, 1]][i]
                if (density_1 - density_2 >= self.__threshold[j]):
                    index += 2**j
            shapes_in_bin[index].append(i)

        prediction = np.zeros_like(regression_targets)
        self.__bin_output = [None] * bin_num
        for i in range(bin_num):
            temp = np.zeros((self.__landmark_num, 2))
            bin_size = len(shapes_in_bin[i])

            for j in range(bin_size):
                idx = shapes_in_bin[i][j]
                temp += regression_targets[idx]

            if bin_size == 0:
                self.__bin_output[i] = temp
                continue

            temp = (1.0 / ((1.0 + 1000.0 / bin_size) * bin_size)) * temp 

            self.__bin_output[i] = temp

            for j in range(bin_size):
                idx = shapes_in_bin[i][j]
                prediction[idx] = temp

        return prediction

    def write(self, fout) -> None:
        fout.write(f"{self.__fern_pixel_num}\n{self.__landmark_num}\n")
        for i in range(self.__fern_pixel_num):
            fout.write(" ".join(list(map(str, self.__selected_pixel_locations[i, :]))))
            fout.write("\n")
            fout.write(
                f"{self.__selected_nearest_landmark_index[i, 0]}\n"
                f"{self.__selected_nearest_landmark_index[i, 1]}\n"
                f"{self.__threshold[i][0]}\n"
            )
        for i in range(len(self.__bin_output)):
            for j in range(self.__bin_output[i].shape[0]):
                fout.write(
                    f"{self.__bin_output[i][j, 0]} {self.__bin_output[i][j, 1]} "
                )
            fout.write("\n")

    def read(self,fin) -> None:
        self.__fern_pixel_num = int(fin.readline().strip())
        self.__landmark_num = int(fin.readline().strip())
        self.__selected_nearest_landmark_index = np.zeros(
            (self.__fern_pixel_num, 2), dtype=int
        )
        self.__selected_pixel_locations = np.zeros((self.__fern_pixel_num, 4))
        self.__threshold = np.zeros((self.__fern_pixel_num, 1))
        self.__bin_output = []
        for i in range(self.__fern_pixel_num):
            pixel_location = list(map(float, fin.readline().strip().split()))
            self.__selected_pixel_locations[i] = pixel_location
            self.__selected_nearest_landmark_index[i, 0] = int(fin.readline().strip())
            self.__selected_nearest_landmark_index[i, 1] = int(fin.readline().strip())
            self.__threshold[i] = float(fin.readline().strip())

        bin_num = 2**self.__fern_pixel_num
        for i in range(bin_num):
            temp = np.zeros((self.__landmark_num, 2))
            outputs = list(map(float, fin.readline().strip().split()))

            temp[:, 0] = outputs[::2]
            temp[:, 1] = outputs[1::2]
            self.__bin_output.append(temp)

    def predict(
        self,
        image: np.ndarray[int],
        shape: np.ndarray[float],
        rotation: np.ndarray[float],
        bounding_box: BoundingBox,
        scale: float,
    ) -> np.ndarray[float]:
        index = 0
        for i in range(self.__fern_pixel_num):
            nearest_landmark_index_1: int = self._selected_nearest_landmark_index[i, 0]
            nearest_landmark_index_2: int = self._selected_nearest_landmark_index[i, 1]
            x = self._selected_pixel_locations[i][0]
            y = self._selected_pixel_locations[i][1]
            project_x: float = scale * (rotation[0, 0] * x + rotation[0, 1] * y) * bounding_box.width / 2.0 + shape[nearest_landmark_index_1, 0]
            project_y: float = scale * (rotation[1, 0] * x + rotation[1, 1] * y) * bounding_box.height / 2.0 + shape[nearest_landmark_index_1, 1]


            project_x = int(max(0, min(project_x, image.shape[1] - 1)))
            project_y = int(max(0, min(project_y, image.shape[0] - 1)))
            intensity_1 = int(image[project_y, project_x])

            x = self._selected_pixel_locations[i][2]
            y = self._selected_pixel_locations[i][3]
            project_x: float = scale * (rotation[0, 0] * x + rotation[0, 1] * y) * bounding_box.width / 2.0 + shape[nearest_landmark_index_2, 0]
            project_y: float = scale * (rotation[1, 0] * x + rotation[1, 1] * y) * bounding_box.height / 2.0 + shape[nearest_landmark_index_2, 1]

            project_x: int = int(max(0, min(project_x, image.shape[1] - 1.)))
            project_y: int = int(max(0, min(project_y, image.shape[0] - 1.)))
            intensity_2 = int(image[project_y, project_x])

            if intensity_1 - intensity_2 >= self.__threshold[i]:
                index += 2**i

        return self.__bin_output[index]
