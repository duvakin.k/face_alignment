class BoundingBox:
    def __init__(
        self,
        start_x: float = 0.0,
        start_y: float = 0.0,
        width: float = 0.0,
        height: float = 0.0,
    ) -> None:
        self.start_x = start_x
        self.start_y = start_y
        self.width = width
        self.height = height
        self.centroid_x = start_x + width / 2
        self.centroid_y = start_y + height / 2
