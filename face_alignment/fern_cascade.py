import time
from typing import List

import numpy as np

from face_alignment.bounding_box import BoundingBox
from face_alignment.fern import Fern
from face_alignment.utils import (
    calculate_covariance,
    project_shape,
    similarity_transform,
)


class FernCascade:

    __slots__ = ["__ferns", "__second_level_num"]

    def train(
        self,
        images: List[np.ndarray[int]],
        current_shapes: List[np.ndarray[float]],
        ground_truth_shapes: List[np.ndarray[float]],
        bounding_box: List[BoundingBox],
        mean_shape: np.ndarray[float],
        second_level_num: int,
        candidate_pixel_num: int,
        fern_pixel_num: int,
        curr_level_num: int,
        first_level_num: int,
    ) -> List[np.ndarray[float]]:
        candidate_pixel_locations = np.zeros((candidate_pixel_num, 2))
        nearest_landmark_index = np.zeros((candidate_pixel_num, 1))
        regression_targets = [np.ndarray([])] * len(current_shapes)
        self.__second_level_num = second_level_num

        for i in range(len(current_shapes)):
            regression_targets[i] = project_shape(ground_truth_shapes[i], bounding_box[i]) - project_shape(current_shapes[i], bounding_box[i])

            rotation, scale = similarity_transform(
                mean_shape, 
                project_shape(current_shapes[i], bounding_box[i])
            )
            regression_targets[i] = scale * regression_targets[i] @ rotation.T

        
        for i in range(candidate_pixel_num):
            x = np.random.uniform(low=-1, high=1)
            y = np.random.uniform(low=-1, high=1)
            while x**2 + y**2 > 1:
                x = np.random.uniform(low=-1, high=1)
                y = np.random.uniform(low=-1, high=1)
            min_index = np.argmin(
                [((row[0] - x) ** 2 + (row[1] - y) ** 2) for row in mean_shape]
                )
            candidate_pixel_locations[i, 0] = x - mean_shape[min_index, 0]
            candidate_pixel_locations[i, 1] = y - mean_shape[min_index, 1]

            nearest_landmark_index[i] = min_index

        densities = [[] for _ in range(candidate_pixel_num)]

        for i in range(len(images)):
            temp: np.ndarray[float] = project_shape(current_shapes[i], bounding_box[i])
            rotation, scale = similarity_transform(temp, mean_shape)
            for j in range(candidate_pixel_num):
                project_x = rotation[0, 0] * candidate_pixel_locations[j, 0] + rotation[0, 1] * candidate_pixel_locations[j, 1]
                project_y = rotation[1, 0] * candidate_pixel_locations[j, 0] + rotation[1, 1] * candidate_pixel_locations[j, 1]
                project_x *= scale * bounding_box[i].width / 2
                project_y *= scale * bounding_box[i].height / 2 
                idx: int = int(nearest_landmark_index[j])
                real_x = project_x + current_shapes[i][idx, 0]
                real_y = project_y + current_shapes[i][idx, 1]
                real_x = int(max(0, min(real_x, images[i].shape[1] - 1)))
                real_y = int(max(0, min(real_y, images[i].shape[0] - 1)))
                densities[j].append(int(images[i][real_y, real_x]))

        covariance = np.zeros((candidate_pixel_num, candidate_pixel_num))

        for i in range(candidate_pixel_num):
            for j in range(i, candidate_pixel_num):
                covariance[i, j] = covariance[j, i] = calculate_covariance(densities[i], densities[j])

        prediction = [np.zeros((mean_shape.shape[0], 2))] * len(regression_targets)

        self.__ferns = [Fern()] * second_level_num
        t = time.perf_counter()
        for i in range(second_level_num):
            temp = self.__ferns[i].train(
                densities,
                covariance,
                candidate_pixel_locations,
                nearest_landmark_index,
                regression_targets,
                fern_pixel_num,
            )
            for j in range(len(temp)):
                prediction[j] += temp[j]
                regression_targets[j] -= temp[j]

            if (i + 1) % 50 == 0:
                print(f"Fern cascades: {curr_level_num} out of {first_level_num};")
                print(f"Ferns: {i + 1} out of {second_level_num};")
                remaining_level_num = (
                    (first_level_num - curr_level_num) * second_level_num
                    + second_level_num
                    - i
                )
                time_remaining = 0.02 * (time.perf_counter() - t) * remaining_level_num
                print(
                    f"Expected remaining time: {int(time_remaining/60)} min {int(time_remaining%60)} sec"
                )
                t = time.perf_counter()

        for i in range(len(prediction)):
            rotation, scale = similarity_transform(
                project_shape(current_shapes[i], bounding_box[i]), mean_shape
            )
            prediction[i] = scale * prediction[i] @ rotation.T

        return prediction

    def read(self, fin) -> None:
        self.__second_level_num = int(fin.readline().strip())
        self.__ferns = [Fern() for _ in range(self.__second_level_num)]
        for fern in self.__ferns:
            fern.read(fin)

    def write(self, fout) -> None:
        fout.write(f"{self.__second_level_num}\n")
        for fern in self.__ferns:
            fern.write(fout)

    def predict(
        self,
        image: np.ndarray[int],
        bounding_box: BoundingBox,
        mean_shape: np.ndarray[float],
        shape: np.ndarray[float],
    ) -> np.ndarray[float]:
        result = np.zeros((shape.shape[0], 2))
        rotation, scale = similarity_transform(
            project_shape(shape, bounding_box), mean_shape
        )
        for i in range(self.__second_level_num):
            result += self.__ferns[i].predict(
                image, shape, rotation, bounding_box, scale
            )
        rotation, scale = similarity_transform(
            project_shape(shape, bounding_box), mean_shape
        )
        return scale * result @ rotation.T
