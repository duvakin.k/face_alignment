from face_alignment import FaceAlignment
from face_alignment.settings import settings


if __name__ == "__main__":
    model = FaceAlignment(name="my_awesome_model", settings=settings)
    model.run()
